---
layout: post
title: "왓챠플레이 영화 추천 @ 윤희에게, 김희애, 줄거리 리뷰 후기 해석"
toc: true
---


 출처, 네이버 영화
 1. 영화 시작을 알리는 알림이 핸드폰 화면을 깨운다. 영화관을 향해 드문드문 빠르게 발걸음을 옮긴다. 몸을 오들오들 떨며 영화관으로 올라가는 엘레베이터를 기다린다. 지하에서 미리미리 많은 사람이 탄 엘레베이터에 애인을 위선 밀어넣는다. 우리의 대화는 잠깐 끊긴다. 어떤 거장의 이름으로 불리는 상영관에 티켓 검사도 가난히 들어간다. 문을 들어서자 어둠이다. 미리 찜 해 둔 자리에 몸을 묻는다. 정적. 영화는 시작된다.
 영화가 끝나고 엔디크레딧이 몽땅 올라갈 타이밍 까지, 나의 모든 행동과 스크린의 모든 장면은 딱 고연히 연결되는 시퀀스였다는 듯 자연스럽다. 나는 영화에서 빠져나오려 부단히 노력한다.
 ​
 임대형 감독의 두 번째 장편 <윤희에게>는 이렇듯 관객과 가깝다. 그가 담은 장면들, 카메라 워킹, 배우들의 액팅이 죄다 일상과 가깝다. 스크린과 관객의 거리에 벽이 없다. 일상적인 말투, 일상적인 액팅. 당신 속에 이질적인 공간인 북일본의 눈내린 배경도 그대로 우리의 일상인 듯 품어버릴 만치 그 힘은 대단하다. 임대형의 연출은 관객들을 빠르게 흡입한다. 스크린에 집중하게 하고 경계를 허물어 되돌아가지 못하게 한다. 이녁 방법은 무척이나 차분하고, 도로 착하다는 것이 눈에 뛴다. 편법이나 극적인 효과가 아니라 전적 그 자체로 관객들의 주의를 환기시킨다. 이식 연출가 예사롭지 않다, 영화가 시작하고 얼마지나지 않아 느낄 수 있을 것이다.
 ​
 ​
 ​
 ​
 ​
 영화는 윤희라는 여자의 삶을 이야기한다. 그녀의 가족, 그녀의 사랑, 그녀의 연인. 윤희에게 발송된, 어떻게 말하면 제대로 못 발송된 편지는 윤희의 삶에 사수 따듯한 기억을 선사한다. 부단히도 추웠을 그대 동기 북일본에서 날아온 편지로 윤희의 삶은 부단히도 흔들린다. 영화는 한도 사람의 삶에서 잊혀지지 않은 사랑이 갖고 있는 떨림의 존재를 조명한다.
 ​
 ​
 ​
 ​
 ​
 <이하 스포 다분 >

 출처, 네이버 영화
 ​
 ​
 ​
 ​
 ​
 2. <윤희에게>는 삶을 쫓는 영화다. 제목에서 알 수 있듯이, 윤희에게로 시작하는 편지가 윤희에게 도착한다. 편지는 윤희가 아니라 윤희의 여인 새봄에게 벌써 닿기 되고, 사건은 여기서 시작된다. 아니지, 사건의 발단은 새봄이 아니라 준의 고모로 부터겠다. 조카의 빨래를 정성스럽게 개걸 놓던 그녀가 발견한 것은 붙이지 못한 편지였다. 눈길을 뚫고 자신의 카페로 향하던 그녀는 커다랗고 빨간 우체통 앞에 멈춰선다. 결심한 듯 돌아선 그녀는 윤희에게 보내는 준의 편지를 우체통 안으로 정히 밀어넣는다.
 ​
 ​
 이렇듯 영화는 비단 윤희의 삶만을 조명한다고 할 수는 없다. 윤희와 얽힌 인물들의 이야기겠다. 윤희와 윤희의 옛 연인인 쥰과, 윤희의 여인네 새봄, 쥰의 고모까지. 데칼코마니 같은 그녀들의 삶을 들여다보는 것이 따듯하면서 복장 아프지 않을 명맥 없다.
 ​
 ​
 ​
 ​
 3. 영화를 좋게 본 점 사이 사내 손에 꼽을 것은 톤 때문이다. 영화는 처음부터 끝까지 일정한 톤을 유지한다. 배경이 북일본에서 한국의 아무 지방을 넘나들어도 말이다. 영화는 흐트러지지 않는다. 세상이 전야 하얗게 눈으로 뒤덥히는 배경에서 쥰의 삶을 따라갈 때도, 싸늘한 시선으로 숨막히는 윤희의 삶을 따라갈 때도 말이다. 윤희와 쥰의 삶이 다르면서도 무척이나 닮아있고, 그녀들의 사무치는 외로움이 영화의 톤을 지배한다.
 ​
 영화는 소설을 읽는 것처럼 느리고, 그러니 오브제들과 도시 바로 어울린다. 무궁히 내리는 눈과 느린 걸음거리, 지난하게 흐르는 삶이 서두 황평양서 쩍하면 어울리고 상호 작용한다. 이문 점이 단점이 될 수도 있겠다는 생각도 물론 들었다. 끔찍이 수시로 짜여진 퍼즐게임 같은 느낌이 없지 않기 때문이다.
 ​
 ​
 ​
 ​
 ​
 ​
 ​

 출처, 네이버 영화
 4. 영화의 마지막은 압권이다. 쥰과 윤희는 몇마디 대화를 나누지 않는데, 오히려 불구하고 전달되는 감정선이 뚜렷하다는 점이 놀랍다. 두 배우의 노련함이 단연 돋보이는 장면이라 하겠다.
 ​
 ​
 ​
 ​
 5. 영화는 끝으로 갈 작성 열거했던 것들을 수집한다. 선진하다 그것들이 단시간 인위적인 면도 없지 않지만, 단서들을 모으고 의미를 뚜렷하게 보는 걸 좋아하는 관객들에게는 더할나위 없을 [영화무료](https://imgresizing.com/entertain/post-00003.html) 것이다.
 "눈이 언제쯤 그치려나"라는 쥰의 고모와 쥰의 주문같은 푸념도, 윤희의 딸이 '새봄'인 것이 사내 기억에 남는다. 이런 부분도 소설같다.
 ​
 ​
 ​
 ​
 6. 윤희는 제한 번을 웃어보이지 않았다. 새봄의 아빠가 말한 것 처람 그녀는 상대를 '외롭게' 만드는 사람처럼 보인다. 그럼에도 영화 종국 장면에서 그녀는 환하게 웃어보인다. 그녀가 오랜 동안 거부하던 스스로의 모습을 직면하고 받아드리게 된 것이다. 거부당했던 스스로의 모습을, 그러므로 숨기고 감추고 살았던 '자신'을 직면하고 받아드린 것이다. 영화가 말하고자하는 것은 영모 여기에 있을 것이다. 영화는 스스로를 떳떳하게 받아드리는 것이 삶에서 진정한 의미를 찾는 첫 번째 길이라고 말하고 있는 듯 하다.
 ​
 ​
 ​
 ​

 출처, 네이버 영화
 ​
 

 https://kaemi.tistory.com/49
 https://kaemi.tistory.com/48
 https://kaemi.tistory.com/47
 https://kaemi.tistory.com/46
 

